## Exercise

A tiny counter App that should have:

- Display with the value of the counter
- Counter controls (Increase and decrease counter)
- Counter step (How much should the counter increase and decrease)
